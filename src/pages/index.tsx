import Head from 'next/head'
import {Container} from "react-bootstrap";
import {useEffect, useRef, useState} from "react";
import Header from "@/components/Header/Header";
import styles from './../styles/Home.module.scss'
import Main from "@/components/Main/Main";
import ContactUs from "@/components/Header/ContactUs";
import CustomScrollBar from "@/components/CustomScrollBar/CustomScrollBar";
import Services from "@/components/Services/Services";
import About from "@/components/About/About";
import Contacts from "@/components/Contacts/Contacts";
import Gallery from "@/components/Gallery/Gallery";
import Map from "@/components/Contacts/Map";
import {useAppDispatch} from "@/app/hooks";
import {setContactHeader} from "@/store/siteStore";
import Footer from "@/components/Footer/Footer";

export default function Home() {
    const [modalShow, setModalShow] = useState<boolean>(false)
    const hideModal = () => {
        setTimeout(() => {
            dispatch(setContactHeader('Здравствуйте, куда можно Вам перезвонить?'))
        }, 1000)
        setModalShow(false)
    }
    const showModal = () => setModalShow(true)
    const scrollContainer = useRef(null)
    const contacts = useRef(null)
    const serviceRef = useRef(null)
    const about = useRef(null)
    const gallery = useRef(null)
    const main = useRef(null)
    const dispatch = useAppDispatch()


    useEffect(() => {
        // Выполнять код после 2 секунд
        const timeout = setTimeout(() => {
            dispatch(setContactHeader('Оставь заявку сейчас и получи скидку 10%'))
            showModal()
        }, 5000)

        // Очистить таймаут при размонтировании компонента
        return () => clearTimeout(timeout);
    }, []);


    return (
        <>
            <Head>
                <title>Центр пескоструйно обработки и покраски</title>
                <meta name="description" content="Центр пескоструйно обработки и покраски"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <meta name="yandex-verification" content="e2a0ef85b650e764" />
                <link rel="icon" href={"/favicon.ico"}/>
            </Head>
            {/*@ts-ignore*/}
            <CustomScrollBar ref={scrollContainer}>
                <div className={styles.mainContainer}>
                    <Container>
                        <Header refs={{
                            container: scrollContainer,
                            contacts: contacts,
                            services: serviceRef,
                            gallery: gallery,
                            about: about,
                            main: main
                        }} showModal={showModal}/>
                    </Container>
                    {/*@ts-ignore*/}
                    <Main ref={main} showModal={showModal}/>
                    {/*@ts-ignore*/}
                    <Services ref={serviceRef} showModal={showModal}/>
                    {/*@ts-ignore*/}
                    <Gallery ref={gallery}/>
                    {/*@ts-ignore*/}
                    <About ref={about}/>
                    <Container ref={contacts}>
                        <h2>Контакты</h2>
                        <Map />
                    </Container>
                    {/*@ts-ignore*/}
                    <Contacts />
                    <hr style={{color: '#ee9b00'}}/>
                    <Footer />
                </div>
            </CustomScrollBar>
            <ContactUs show={modalShow} onHide={hideModal}/>
        </>
    )
}
