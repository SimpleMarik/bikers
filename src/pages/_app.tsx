// import 'bootstrap/dist/css/bootstrap.min.css';
import '@/styles/globals.scss'
import type {AppProps} from 'next/app'
import {ThemeProvider} from "react-bootstrap";
import {store} from "@/app/store";
import {Provider} from "react-redux";
import localFont from 'next/font/local'

const tildaSans = localFont({
    src: [
        {
            path: './fonts/TildaSans-Regular.ttf',
            weight: '700',
            style: 'normal',
        },
        {
            path: './fonts/TildaSans-Bold.ttf',
            weight: '800',
            style: 'normal',
        },
        {
            path: './fonts/TildaSans-Medium.ttf',
            weight: '700',
            style: 'normal',
        }
    ]
})

export default function App({Component, pageProps}: AppProps) {
    return (
        <main className={tildaSans.className}>
            <ThemeProvider
                breakpoints={['xxl', 'xl', 'lg', 'md', 'sm', 'xs', 'xxs']}
                minBreakpoint="xxs">
                <Provider store={store}>
                    <Component {...pageProps} />
                </Provider>
            </ThemeProvider>
        </main>
    )
}
