import {configureStore, ThunkAction, Action} from '@reduxjs/toolkit';
import notificationStore from "../store/notificationStore";
import defaultStore from '../store/siteStore'

export const store = configureStore({
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: false,
        }),
    reducer: {
        siteStore: defaultStore,
        notifications: notificationStore
    },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType,
    RootState,
    unknown,
    Action<string>>;
