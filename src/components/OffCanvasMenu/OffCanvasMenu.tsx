import React, {FC} from "react";
import {Nav, Offcanvas} from "react-bootstrap";
import styles from './OffCanvasMenu.module.scss'

interface OffCanvasMenuProps {
    show: boolean
    handleClose: () => void
    refs: any
}

const OffCanvasMenu: FC<OffCanvasMenuProps> = ({show, handleClose, refs}) => {
    const scrollTo = (type: string) => {
        refs.container?.current.scrollTo({
            top: refs[type].current.offsetTop - 90
        })
        handleClose()
    }

    return (
        <>
            <Offcanvas scroll={true} show={show} onHide={handleClose}>
                <Offcanvas.Header className={styles.offCanvas} closeButton>
                    <Offcanvas.Title>Меню</Offcanvas.Title>
                </Offcanvas.Header>
                <Offcanvas.Body className={styles.offCanvasBlock}>
                    <Nav.Link className={styles.offCanvasText} onClick={() => scrollTo('main')}>Главная</Nav.Link>
                    <Nav.Link className={styles.offCanvasText} onClick={() => scrollTo('services')}>Услуги</Nav.Link>
                    <Nav.Link className={styles.offCanvasText} onClick={() => scrollTo('about')}>О нас</Nav.Link>
                    <Nav.Link className={styles.offCanvasText} onClick={() => scrollTo('contacts')}>Контакты</Nav.Link>
                    <Nav.Link className={styles.offCanvasText} onClick={() => scrollTo('gallery')}>Галерея</Nav.Link>
                </Offcanvas.Body>
            </Offcanvas>
        </>
    )
}

export default React.memo(OffCanvasMenu)