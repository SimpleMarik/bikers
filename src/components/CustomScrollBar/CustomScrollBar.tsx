import React, {FC, forwardRef, useEffect} from 'react';
import styles from './CustomScrollBar.module.scss'
interface CustomScrollbarChildren {
    children: any,
}
const CustomScrollBar: FC<CustomScrollbarChildren> = forwardRef<HTMLDivElement, CustomScrollbarChildren>(({children}, ref) => {
    return (
        <div ref={ref} id={'CustomScrollBar'} className={`${styles.scrollContainer} 1`}>
            <div className={styles.scrollContent}>
                {children}
            </div>
        </div>
    );
});

CustomScrollBar.displayName = 'CustomScrollBar'

export default React.memo(CustomScrollBar);