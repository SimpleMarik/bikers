import React, {FC, forwardRef} from "react";
import {Col, Row} from "react-bootstrap";
import styles from './Contacts.module.scss'

const Contacts: FC = forwardRef<HTMLDivElement>(() => {
    return (
            <Row>
                <Col>
                    <iframe
                        className={styles.contactsMap}
                        src="https://yandex.ru/map-widget/v1/?um=constructor%3A0e9df0b5d370b00f20987131cc1d4206e0e433e2a9eb34ac9044cda0bd066c0e&amp;source=constructor"
                        width="100%" height="347"></iframe>
                </Col>
            </Row>
    )
})

Contacts.displayName = 'Contacts'

export default React.memo(Contacts)