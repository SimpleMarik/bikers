import React, {FC, forwardRef, useState} from "react";
import {Col, Container, Row} from "react-bootstrap";
import styles from './Contacts.module.scss'
import PhoneInput from "@/components/PhoneInput";
import formStyles from '../Header/ContactUs.module.scss'
import {send} from "@/components/Header/ContactUs";

const Contacts: FC = forwardRef<HTMLDivElement>((props, ref) => {
    const [nameValue, setNameValue] = useState<string>('');
    const [phone, setPhone] = useState<string | null>('')
    const setPhoneValue = (phone: string | null) => {
        setPhone(phone)
    }
    const [sent, setSent] = useState(false)
    const handleSent = () => {
        setSent(true)
    }
    const [pending, setPending] = useState(false)
    return (
        <div ref={ref} id={'contacts'} className={styles.services}>
            <Container>
                <h4 style={{textAlign: "center", marginBottom: '20px'}}>Нужна бесплатная консультация по обработке вашей поверхности?</h4>
                <Row>
                    <Col>
                        <div className={styles.contacts}>
                            <Row>
                                <Col>
                                    <Row>
                                        <Col md={6}>
                                            <div className={styles.contactsInfo}>
                                                <p className={styles.contactsText}>
                                                    Адрес: г. Москва, ул. Шоссейная 59, стр 2
                                                </p>
                                                <p className={styles.contactsText}>
                                                    Телефон: +7 (925) 607-33-39
                                                </p>
                                                <p style={{marginBottom: 0}} className={styles.contactsText}>
                                                    Телефон: +7 (919) 773-39-13
                                                </p>
                                            </div>
                                        </Col>
                                        <Col md={6}>
                                            {!sent && !pending ? <div className={styles.contactsInfo}>
                                                <input maxLength={30} placeholder={'Ваше имя'} onChange={(e) => {
                                                    setNameValue(e.target.value)
                                                }
                                                } className={formStyles.input} type="text" value={nameValue}/>
                                                <PhoneInput setValue={setPhoneValue} className={formStyles.input}/>
                                                <button onClick={async () => {
                                                    if (nameValue && phone) {
                                                        setPending(true)
                                                        await send(nameValue, phone, handleSent)
                                                        setPending(false)
                                                    }
                                                }}
                                                        style={{width: '100%'}} className={formStyles.button}>
                                                    <p>
                                                        Рассчитайте стоимость покраски
                                                    </p>
                                                </button>
                                            </div> : pending ? <p style={{color: "white", display: "flex", alignItems: "center", justifyContent: "center", marginTop: 0}} className={formStyles.button}>Отправка...</p>
                                                : <p style={{color: "white", display: "flex", alignItems: "center", justifyContent: "center", marginTop: 0}} className={formStyles.button}>Отправлено</p>}
                                        </Col>
                                    </Row>

                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>

    )
})

Contacts.displayName = 'Contacts'

export default React.memo(Contacts)