import React, {FC} from 'react';
import InputMask from 'react-input-mask';
interface PhoneInputProps {
    className: string
    setValue: (value: string) => void
}
const PhoneInput:FC<PhoneInputProps> = ({...props}: PhoneInputProps) => {
    return(
        <InputMask onChange={(e) => {
            props.setValue(e.target.value)
        }} placeholder={'+7 (999) 999 99 99'} className={props.className} mask="+7 (999) 999 99 99" maskChar=" " />
    )
}

export default React.memo(PhoneInput)
