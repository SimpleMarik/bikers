import React, {FC, forwardRef} from "react";
import {Col, Container, Row} from "react-bootstrap";
import styles from './Main.module.scss'

type MainProps = {
    showModal: () => void
}
const Main: FC<MainProps> = forwardRef<HTMLDivElement, MainProps>(({...props}: MainProps, ref) => {
    return (
        <div ref={ref} className={styles.mainScreenBack}>
            <Container>
                <Row className={styles.mainScreen}>
                    <Col className={`col-12`} lg={7}>
                        <h1 className={styles.h1}>
                            Центр <span className={styles.span}>пескоструйной обработки</span> и <span className={styles.span}>порошковой покраски</span> в Москве
                        </h1>
                        <p className={`${styles.mainColText}`}>
                            Мы используем только высококачественные порошковые материалы и современное оборудование. Берем работы любой сложности. Гарантируем идеальный результат.
                        </p>
                        <p className={`${styles.mainColText}`}>
                            Работаем в Москве и области с частными и промышленными заказами
                        </p>
                        <button onClick={() => props.showModal()}
                                className={`${styles.servicesButton}`}>
                            <p style={{width: '100%'}}>
                                Рассчитайте стоимость покраски
                            </p>
                        </button>
                    </Col>
                    <Col className={`col-12 ${styles.subContainer}`} lg={5}>
                        {/*<div className={styles.circles}>*/}
                        {/*    <div className={`${styles.circle} ${styles.circle1}`}></div>*/}
                        {/*    <div className={`${styles.circle} ${styles.circle2}`}></div>*/}
                        {/*    <div className={`${styles.circle} ${styles.circle3}`}></div>*/}
                        {/*</div>*/}

                    </Col>
                </Row>
            </Container>
        </div>
    )
})

Main.displayName = 'Main'

export default React.memo(Main)
