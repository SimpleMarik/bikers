import React, {FC, forwardRef} from "react";
import {Col, Container, Image, Row} from "react-bootstrap";
import styles from './Services.module.scss'
import clear from '../../assets/pesk.jpg'
import paint from '../../assets/paint.jpg'
import Contacts from "@/components/Contacts/Contacts";

type ServicesProps = {
    showModal: () => void
}
const Services: FC<ServicesProps> = forwardRef<HTMLDivElement, ServicesProps>(({...props}: ServicesProps, ref) => {
    // const [switcher, setSwitcher] = useState(false)
    return (
        <div ref={ref} className={styles.services}>
            <Container>
                <Row>
                    <Col>
                        <h2>
                            Наши услуги
                        </h2>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <div className={styles.servicesInfo}>
                            {/*<Row className={styles.servicesButtons}>*/}
                            {/*    <Col className={styles.servicesButtonsContainer}>*/}
                            {/*        <button onClick={() => setSwitcher(false)}*/}
                            {/*                className={`${styles.servicesButton} ${!switcher ? styles.servicesButtonActive : ''}`}>*/}
                            {/*            <p style={{width: '100%'}}>*/}
                            {/*                Пескоструйная очистка*/}
                            {/*            </p>*/}
                            {/*        </button>*/}
                            {/*        <button onClick={() => setSwitcher(true)}*/}
                            {/*                className={`${styles.servicesButton} ${switcher ? styles.servicesButtonActive : ''}`}>*/}
                            {/*            <p style={{width: '100%'}}>*/}
                            {/*                Порошковая покраска и полимеризация*/}
                            {/*            </p>*/}
                            {/*        </button>*/}
                            {/*    </Col>*/}
                            {/*</Row>*/}

                            <h3 style={{margin: '0 0 30px 0', textAlign: "center"}}>Пескоструйная очистка</h3>

                            <Row>
                                <Col style={{display: "flex", alignItems: 'center', justifyContent: "center"}}
                                     lg={6}>
                                    <Image alt={'Пескоструйная очистка'} className={styles.serviceImage}
                                           src={clear.src}/>
                                </Col>
                                <Col lg={6}>
                                    <p style={{fontSize: '20px', textAlign: "left", marginBottom: '20px'}}>
                                        Пескоструйная очистка это холодная обработка поверхности пескоструйным
                                        абразивом. Быстро и качественно удаляет въевшиеся загрязнения, ржавчину, старые
                                        отложения, обновляет материал и подготавливает поверхность к дальнейшей
                                        обработке/ покраске.
                                    </p>
                                    <p style={{fontSize: '20px', textAlign: "left", marginBottom: '20px'}}>
                                        Осуществляем очистку следующих видов поверхностей:
                                    </p>
                                    <ul className={styles.servicesList}>
                                        <li>
                                            Бетон
                                        </li>
                                        <li>
                                            Камень
                                        </li>
                                        <li>
                                            Кирпич
                                        </li>
                                        <li>
                                            Сталь
                                        </li>
                                        <li>
                                            Древесина
                                        </li>
                                        <li>
                                            Металл
                                        </li>
                                        <li>
                                            Стены
                                        </li>
                                        <li>
                                            Здания
                                        </li>
                                        <li>
                                            Гидрофоб
                                        </li>
                                    </ul>
                                    {/*<button style={{width: '310px'}} onClick={() => props.showModal()}*/}
                                    {/*        className={`${styles.servicesButton}`}>*/}
                                    {/*    <p style={{width: '100%'}}>*/}
                                    {/*        Оставить заявку*/}
                                    {/*    </p>*/}
                                    {/*</button>*/}
                                </Col>
                            </Row>

                        </div>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <div className={styles.servicesInfo}>
                            {/*<Row className={styles.servicesButtons}>*/}
                            {/*    <Col className={styles.servicesButtonsContainer}>*/}
                            {/*        <button onClick={() => setSwitcher(false)}*/}
                            {/*                className={`${styles.servicesButton} ${!switcher ? styles.servicesButtonActive : ''}`}>*/}
                            {/*            <p style={{width: '100%'}}>*/}
                            {/*                Пескоструйная очистка*/}
                            {/*            </p>*/}
                            {/*        </button>*/}
                            {/*        <button onClick={() => setSwitcher(true)}*/}
                            {/*                className={`${styles.servicesButton} ${switcher ? styles.servicesButtonActive : ''}`}>*/}
                            {/*            <p style={{width: '100%'}}>*/}
                            {/*                Порошковая покраска и полимеризация*/}
                            {/*            </p>*/}
                            {/*        </button>*/}
                            {/*    </Col>*/}
                            {/*</Row>*/}

                            <h3 style={{margin: '0 0 30px 0', textAlign: "center"}}>Порошковая покраска</h3>
                            <Row>
                                <Col style={{display: "flex", alignItems: 'center', justifyContent: "center"}}
                                     lg={6}>
                                    <Image alt={'Порошковая покраска'} className={styles.serviceImage} src={paint.src}/>
                                </Col>
                                <Col lg={6}>
                                    <p style={{fontSize: '20px', textAlign: "left", marginBottom: '20px'}}>
                                        Порошковая покраска исключает человеческий фактор и гарантирует равномерный слой
                                        нанесения краски без подтеков. Порошковые краски рекомендуется наносить на
                                        детали, подвергающиеся агрессивным воздействиям внешней среды, они не
                                        скалываются и не отслаиваются. Гораздо дольше других сохраняет свои эстетические
                                        и защитные свойства.
                                    </p>
                                    <ul className={styles.servicesList}>
                                        <li>
                                            Металлоконструкции
                                        </li>
                                        <li>
                                            Алюминиевый профиль
                                        </li>
                                        <li>
                                            Авто/мото детали
                                        </li>
                                        <li>
                                            Сетки/решетки
                                        </li>
                                        <li>
                                            Радиаторы/батареи
                                        </li>
                                        <li>
                                            Изделий из труб
                                        </li>
                                        <li>
                                            Профнастил
                                        </li>
                                        <li>
                                            Профиль
                                        </li>
                                        <li>
                                            Кованые изделия
                                        </li>
                                        <li>
                                            Рамы велосипеда
                                        </li>
                                        <li>
                                            Диски
                                        </li>
                                        <li>
                                            Двери
                                        </li>
                                        <li>
                                            Фурнитура, детали крепежа и другие изделия размером менее 50мм
                                        </li>
                                    </ul>
                                    {/*<button style={{width: '310px'}} onClick={() => props.showModal()}*/}
                                    {/*        className={`${styles.servicesButton}`}>*/}
                                    {/*    <p style={{width: '100%'}}>*/}
                                    {/*        Оставить заявку*/}
                                    {/*    </p>*/}
                                    {/*</button>*/}
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
                <div style={{
                    marginLeft: '-12px',
                    marginRight: '-12px',
                }}>
                    <Contacts />
                </div>
            </Container>
        </div>

    )
})

Services.displayName = 'Services'

export default React.memo(Services)