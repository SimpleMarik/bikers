import React, {FC, useState} from "react";
import {Modal} from "react-bootstrap";
import styles from './ContactUs.module.scss'
import PhoneInput from "@/components/PhoneInput";
import {useAppSelector} from "@/app/hooks";
import {getContactHeader} from "@/store/siteStore";

type ContactUsProps = {
    show: boolean
    onHide: () => void
}

export const send = async (nameValue: string, phone: string, setOk: () => void) => {
    const res = await fetch('https://polymer-blast.ru/mailer/', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: nameValue,
            phone: phone
        })
    })
    if (res.ok) {
        setOk()
    }
}

const ContactUs: FC<ContactUsProps> = (props: ContactUsProps) => {
    const [nameValue, setNameValue] = useState<string>('');
    const [phone, setPhone] = useState<string | null>('')
    const [pending, setPending] = useState(false)
    const contactHeader = useAppSelector(getContactHeader)
    const setPhoneValue = (phone: string | null) => {
        setPhone(phone)
    }
    const [sent, setSent] = useState(false)
    const handleSent = () => {
        setSent(true)
    }

    return (
        <Modal
            {...props}
            aria-labelledby="contained-modal-title-vcenter"
            centered
            className={`${styles.contactModal}`}
        >
            <Modal.Body className={styles.modalBody} >
                {!sent && !pending ? <><h4 className={styles.header}>
                    {contactHeader}
                </h4>
                    <input maxLength={30} placeholder={'Ваше имя'} onChange={(e) => {
                        setNameValue(e.target.value)
                    }
                    } className={styles.input} type="text" value={nameValue}/>
                    <PhoneInput setValue={setPhoneValue} className={styles.input}/>
                    <button onClick={async () => {
                        if (nameValue && phone) {
                            setPending(true)
                            await send(nameValue, phone, handleSent)
                            setPending(false)
                        }
                    }} className={styles.button}>
                        <p>
                            Рассчитайте стоимость покраски
                        </p>
                    </button></> : pending ? <p style={{color: "white", display: "flex", alignItems: "center", justifyContent: "center"}} className={styles.button}>Отправка...</p>
                    : <p style={{color: "white", display: "flex", alignItems: "center", justifyContent: "center"}} className={styles.button}>Отправлено</p>}
            </Modal.Body>
        </Modal>
    );
}

export default React.memo(ContactUs)
