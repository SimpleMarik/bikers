import React, {FC, useState} from "react";
import {Col, Container, Row} from "react-bootstrap";
import styles from './Header.module.scss'
import {PinIcon} from "@/components/Icons/PinIcon";
import {MenuIcon} from "@/components/Icons/MenuIcon";
import {PhoneIcon} from "@/components/Icons/PhoneIcon";
import OffCanvasMenu from "@/components/OffCanvasMenu/OffCanvasMenu";

type HeaderProps = {
    showModal: () => void,
    refs: any
}
const Header: FC<HeaderProps> = (props: HeaderProps) => {

    const [showMenu, setShowMenu] = useState(false);
    const handleShow = () => setShowMenu(true);
    const handleClose = () => setShowMenu(false);

    return (
        <div className={styles.header}>
            <Container >
                <Row>
                    <Col onClick={handleShow} style={{alignItems: 'center', display: 'flex', cursor: "pointer"}} className={'col-6'} sm={3}>
                        <p className={styles.headerText}>
                            <MenuIcon width={25} height={25}/>
                            Меню
                        </p>
                    </Col>
                    <Col
                        style={{justifyContent: "center", alignItems: "center"}}
                        className={`${styles.headerAddress} col-6`}
                        sm={5}
                        lg={6}
                    >
                        <a style={{textDecoration: "none"}} target={'_blank'} href="https://yandex.ru/maps/213/moscow/house/shosseynaya_ulitsa_59s2/Z04YcQVhSkcPQFtvfXp3eHhmbA==/?ll=37.721327%2C55.669458&source=serp_navig&z=17.14">
                            <p className={styles.headerText}>
                                <PinIcon width={25} height={35}/>
                                Москва, Шоссейная улица, 59, строение 1
                            </p>
                        </a>
                    </Col>
                    <Col style={{justifyContent: 'end', display: 'flex', alignItems: "center"}} className={'col-6'} lg={3}
                         sm={4}>
                        <div style={{cursor: "pointer"}} onClick={() => props.showModal()} className={styles.headerText}>
                            <PhoneIcon width={20} height={20}/>
                            <div>
                                <Row>
                                    <Col className={`col-12 ${styles.phoneText}`}>
                                        +7 (925) 607-33-39
                                    </Col>
                                </Row>
                                <Row>
                                    <Col className={`col-12 ${styles.subText}`}>
                                        Связаться
                                    </Col>
                                </Row>
                            </div>
                        </div>
                    </Col>
                </Row>
                <OffCanvasMenu show={showMenu} refs={props.refs} handleClose={handleClose}/>
            </Container>
        </div>

    )
}

export default React.memo(Header)
