import React, {FC, forwardRef, useEffect, useState} from "react";
import styles from "./Gallery.module.scss";
import {Col, Container, Image, Row} from "react-bootstrap";
import {Swiper, SwiperSlide} from "swiper/react";
import {Pagination, Navigation} from "swiper/modules";
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';
import GalleryModal from "@/components/Gallery/GalleryModal";

const GalleryComp: FC = forwardRef<HTMLDivElement>(({...props}, ref) => {
    const [images, setArr] = useState<string[] | null>(null)
    const [modalShow, setModalShow] = useState<boolean>(false)
    const [currentImage, setCurrentImage] = useState<string | null>(null)
    const hideModal = () => setModalShow(false)

    useEffect(() => {
        (async () => {
            try {
                const res = await fetch('https://polymer-blast.ru/getimages/')
                const req = await res.json()
                let arr: string[] = [];
                Object.keys(req).forEach(item => {
                    arr.push(req[item]);
                })
                setArr(arr)
            } catch (err) {
                console.log(err)
            }
        })()
    }, [])

    return (
        <div ref={ref} className={styles.gallery}>
            <Container>
                <Row>
                    <Col>
                        <h2>
                            Наши работы
                        </h2>
                    </Col>
                </Row>
                <div className={styles.galleryContainer}>
                    {/*<Carousel className={styles.imageCont}>*/}
                    {/*    {images ? images.map(image => {*/}
                    {/*        return (*/}
                    {/*            <Carousel.Item key={image}>*/}
                    {/*                <Image*/}
                    {/*                    className={styles.image}*/}
                    {/*                    src={`/images/${image}`}*/}
                    {/*                    alt="First slide"*/}
                    {/*                />*/}
                    {/*            </Carousel.Item>*/}
                    {/*        )*/}
                    {/*    }) : null}*/}
                    {/*</Carousel>*/}
                    <GalleryModal image={currentImage} onHide={hideModal} show={modalShow}/>
                    <Swiper
                        slidesPerView={3}
                        spaceBetween={30}
                        // centeredSlides={true}
                        pagination={{
                            clickable: true,
                        }}
                        navigation={true}
                        modules={[Navigation, Pagination]}
                        className={styles.swiper}
                        loop={true}
                        breakpoints={{
                            '@0.00': {
                                slidesPerView: 1,
                                spaceBetween: 10,
                            },
                            '@0.75': {
                                slidesPerView: 2,
                                spaceBetween: 20,
                            },
                            '@1.00': {
                                slidesPerView: 3,
                                spaceBetween: 40,
                            },
                            '@1.50': {
                                slidesPerView: 3,
                                spaceBetween: 40,
                            },
                        }}
                    >
                            {images ? images.map(image => {
                                return (
                                    <SwiperSlide key={image}>
                                        <Image
                                            className={styles.image}
                                            src={`/images/${image}`}
                                            alt="First slide"
                                            onClick={() =>{
                                                setCurrentImage(image)
                                                setModalShow(true)
                                            }}
                                        />
                                    </SwiperSlide>
                                )
                            }) : null}
                        {/*<SwiperSlide>Slide 1</SwiperSlide>*/}
                        {/*<SwiperSlide>Slide 2</SwiperSlide>*/}
                        {/*<SwiperSlide>Slide 3</SwiperSlide>*/}
                        {/*<SwiperSlide>Slide 4</SwiperSlide>*/}
                        {/*<SwiperSlide>Slide 5</SwiperSlide>*/}
                        {/*<SwiperSlide>Slide 6</SwiperSlide>*/}
                        {/*<SwiperSlide>Slide 7</SwiperSlide>*/}
                        {/*<SwiperSlide>Slide 8</SwiperSlide>*/}
                        {/*<SwiperSlide>Slide 9</SwiperSlide>*/}
                    </Swiper>
                </div>
            </Container>
        </div>
    )
})

GalleryComp.displayName = 'Gallery'

export default React.memo(GalleryComp)