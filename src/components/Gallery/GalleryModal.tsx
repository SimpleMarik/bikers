import {CloseButton, Image, Modal} from "react-bootstrap";
import React, {FC} from "react";

type GalleryModalProps = {
    show: boolean
    onHide: () => void
    image: string | null
}
const GalleryModal: FC<GalleryModalProps> = (props: GalleryModalProps) => {
    return (
        <Modal
            {...props}
            size={"xl"}
            centered
            aria-labelledby="contained-modal-title-vcenter"
        >
            <Modal.Body>
                <div style={{position: "absolute", top: '25px', right: '25px'}}>
                    <CloseButton
                        onClick={() => {props.onHide()}}
                        style={{
                            width: '30px',
                            height: '30px',
                            color: 'white',
                        }}
                    />
                </div>
                <Image
                    style={{width: '100%', objectFit: 'contain'}}
                    src={`/images/${props.image}`}
                    alt="First slide"
                />
            </Modal.Body>
        </Modal>
    )
}

export default React.memo(GalleryModal)
