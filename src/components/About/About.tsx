import React, {FC, forwardRef} from "react";
import {Col, Container, Row} from "react-bootstrap";
import styles from './About.module.scss'

const About: FC = forwardRef<HTMLDivElement>(({...props}, ref) => {
    return (
        <div ref={ref} className={styles.about}>
            <Container>
                <Row>
                    <Col>
                        <h2 style={{marginBottom: '20px'}} className={styles.aboutHeader}>
                            О нас
                        </h2>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <div className={styles.flexContainer}>
                            <div className={styles.flexCol}>
                                <p style={{marginTop: 0}} className={styles.aboutText}>
                                    Мы готовы выполнить покраску как отдельных деталей, так и крупных конструкций,
                                    включая металлические ограждения, козырьки, навесы, мебель и многое другое. Наша
                                    команда профессионалов обеспечивает полное соблюдение технологического процесса и
                                    строгий контроль качества работы.
                                </p>
                                <p style={{marginBottom: 0}} className={styles.aboutText}>
                                    Обращаясь к нам, вы получаете высококачественную услугу по доступным ценам. Мы ценим
                                    каждого клиента и готовы выполнить порошковую покраску в удобное для вас время. С
                                    нами ваше металлическое изделие будет выглядеть как новое и прослужит вам долгие
                                    годы. Звоните и заказывайте порошковую покраску у профессионалов!
                                </p>
                            </div>
                            <div className={styles.flexCol}>
                                <p className={styles.aboutText}>
                                    Наша компания предоставляет услуги по пескоструйной очистке металлических изделий
                                    любой сложности. Мы используем только высококачественное оборудование и материалы,
                                    что гарантирует идеальный результат.
                                </p>
                                <p style={{marginBottom: 0}} className={styles.aboutText}>
                                    Мы готовы выполнить пескоструйную очистку как отдельных деталей, так и крупных
                                    конструкций, включая металлические ограждения и многое другое. Наша команда
                                    профессионалов обеспечивает полное соблюдение технологического процесса и строгий
                                    контроль качества работы.
                                </p>
                            </div>
                        </div>

                    </Col>
                </Row>
            </Container>
        </div>

    )
})

About.displayName = 'About'

export default React.memo(About)