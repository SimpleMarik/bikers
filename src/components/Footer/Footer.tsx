import React, {FC} from "react";
import {Col, Container, Row} from "react-bootstrap";
import styles from "@/components/Contacts/Contacts.module.scss";

const Footer:FC = () => {
    return (
        <Container>
            <Row>
                <Col style={{display: "flex", alignItems: "start", flexDirection: "column", justifyContent: "center", textAlign: "start"}}>
                    <div>
                        <p className={styles.footerText}>
                            ООО «РНСК»
                        </p>
                        <p className={styles.footerText}>
                            ИНН 7714436412
                        </p>
                        <p className={styles.footerText}>
                            ОГРН 5187746012801
                        </p>
                        <p className={styles.footerText}>
                            ОГРН 5187746012801
                        </p>
                        <p className={styles.footerText}>
                            {/* eslint-disable-next-line react/no-unescaped-entities */}
                            'Центр пескоструйной обработки и покраски' © 2023. Все права защищены.
                        </p>
                    </div>
                    <div>

                    </div>
                </Col>
            </Row>
        </Container>
    )
}

export default React.memo(Footer)