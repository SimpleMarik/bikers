import {createAsyncThunk, createSlice, PayloadAction} from "@reduxjs/toolkit";
import {RootState} from "@/app/store";
export interface EventServiceState {
    currentLocation: string
    contactHeader: string
}

const initialState: EventServiceState = {
    currentLocation: 'test2',
    contactHeader: 'Здравствуйте, куда можно Вам перезвонить?'
}

export const asyncTest = createAsyncThunk(
    'site/Test',
    async () => {
        return await fetch('')
    }
)

export const SiteStore = createSlice({
    name: 'eventService',
    initialState,
    reducers: {
        setContactHeader: (state, action: PayloadAction<string>) => {
            state.contactHeader = action.payload
        },
        setLocation: (state, action: PayloadAction<string>) => {
            state.currentLocation = action.payload
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(asyncTest.fulfilled, (state, action: any) => {
                state.currentLocation = action
            })
    }
})

export const {
    setContactHeader
} = SiteStore.actions

export const getCurrentLocation = (state: RootState) => state.siteStore.currentLocation
export const getContactHeader = (state: RootState) => state.siteStore.contactHeader
export default SiteStore.reducer
