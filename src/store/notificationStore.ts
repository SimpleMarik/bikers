import {createEntityAdapter, createSlice, isAnyOf} from '@reduxjs/toolkit'
import {RootState} from "@/app/store";
import {asyncTest} from "@/store/siteStore";

const notificationsAdapter = createEntityAdapter()

export interface NotificationState {
    error: string | null | undefined
    success: string | null
}

const initialState: NotificationState = notificationsAdapter.getInitialState({
    error: '',
    success: '',
})

const notificationsSlice = createSlice({
    name: 'notifications',
    initialState,
    reducers: {
        clearNotifications: state => {
            state.error = ''
            state.success = ''
        },
        setError: (state, action) => {
            state.success = null
            state.error = action.payload
        },
        setSuccess: (state, action) => {
            state.success = action.payload
            state.error = null
        },
    },
    extraReducers: builder => {
        builder
            .addMatcher(
                isAnyOf(
                    asyncTest.rejected
                ), (state, action) => {
                    state.error = action.error.message
                    state.success = null
                }
            )
    },
})
export const {clearNotifications, setError, setSuccess} = notificationsSlice.actions

export default notificationsSlice.reducer

export const getErrorMsg = (state: RootState) => state.notifications.error
export const getSuccessMsg = (state: RootState) => state.notifications.success
