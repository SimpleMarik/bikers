/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    PASSWORD: '123'
  },
  experimental: {
    appDir: true,
  }
}

module.exports = nextConfig
