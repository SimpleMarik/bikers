<?php
 // Path to the folder
$folder_path = '/var/www/u2019719/data/www/polymer-blast.ru/images';

// Get a list of filenames in the folder
$filenames = scandir($folder_path);

// Remove . and .. from the array
$filenames = array_diff($filenames, array('.', '..'));

// Encode the filenames as a JSON string
$json = json_encode($filenames);

// Set the response content type to JSON

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type");
header('Content-Type: application/json');

// Send the JSON response
echo $json;
?>